# DFT2FEFFIT

## Presentation

DFT2FEFFIT regresses FEFF EXAFS functions (chip000n) calculated from DFT structure models against experimental EXAFS spectra.

Authors: Romain Brossier (romain.brossier@univ-grenoble-alpes.fr) and Alain Manceau (alain.manceau@ens-lyon.fr)

This code is associated with the article "DFT2FEFFIT: a density-functional-theory-based structural toolkit to analyze EXAFS spectra" published in Journal of Applied Crystallography:
https://doi.org/10.1107/S1600576724005454

## Inputs

The code calls an input text file with the following structure:

File name fo fit (k, chi)

n (integer), number of paths

k (float),  k-weighting of the fitting scheme

kmin kmax (floats), k-range of the fit

S_0^2 (float)

Optimisation of DeltaE (integer) (1), or not (0)

Fixed DeltaE value in eV (float) if option 1, or interval of variation and step if option 0: DeltaEmin(float) DeltaEmax(float) DeltaDeltaE(float)

Path ID (integer); path name (char); path distance (float); name of the chi function for the path (char); format of chi (char), FEFF if chip000n or 2C if (k, chi); opt (integer), 1 if sigma is optimised, 0 if fixed; init (float), initial value of sigma; min (float) value of sigma; max (float) value of sigma; path ID (integer) with which sigma is co-varied, -1 if no co-variation.

Paths starting with "#" are ignored. 

## Examples

One example is provided on this site. Other examples can be found in the original publication.

## Installation

1. Make sure you have Python 3 installed on your computer. We recommend using the Anaconda distribution, which includes many useful packages for scientific computing. You can download it from https://www.anaconda.com/products/distribution.

2. Once the Anaconda distribution is installed, open the `Anaconda Prompt` terminal and install Git: `conda install git`

3. Afterward, type the following command to install the package:
    `pip install --ignore-installed  git+https://gitlab.esrf.fr/scientific-software/dft2feffit.git`. The `--ignore-installed` argument is required if you want to upgrade an existing installation.

4. Once installed, you can run the code by typing `dft2feffit path_to_input_file\input_file.txt` in the terminal. For example, `dft2feffit C:\Users\me\Ce2-Si2-close\Ce2-Si2-close_Durango.txt` The other files required for the calculation are expected to be in the same directory as the input file.
