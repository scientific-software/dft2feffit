#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##############################################################################
###                         DFT2FEFFIT                                     ###
###                Romain Brossier, Alain Manceau                          ###
###                          v1.0 04/2024                                  ###
##############################################################################


import datetime
import os
import pathlib
import sys

import matplotlib.pyplot as plt

# import pandas as pd
import numpy as np
from scipy import signal

from dft2feffit import __version__ as version


def read_inputs(file_as_list, npath):
    index = []
    paths = []
    distance = []
    files = []
    form = []
    opt = []
    sig_init = []
    sig_min = []
    sig_max = []
    cor = []
    print("Read", npath, "input lines.")
    iline = 0
    iread = 0
    while iread < npath:
        line = file_as_list[7 + iline]
        iline = iline + 1
        if line[0] == "#" or line[0] == "!" or line[0] == "$":
            # commented line
            print("One commented line in input ", iline)
        else:
            a, b, c, d, e, f, g, h, i, j = line.split(";")
            index.append(int(a))
            paths.append(str(b))
            distance.append(float(c))
            files.append(str(d))
            form.append(str(e))
            opt.append(int(f))
            sig_init.append(float(g))
            sig_min.append(float(h))
            sig_max.append(float(i))
            cor.append(int(j))
            iread = iread + 1
    for i in range(0, npath):
        print(
            index[i],
            paths[i],
            distance[i],
            files[i],
            form[i],
            opt[i],
            sig_init[i],
            sig_min[i],
            sig_max[i],
            cor[i],
        )
    return index, paths, distance, files, form, opt, sig_init, sig_min, sig_max, cor


def read_2C_file(file, kmin, kmax):
    ktable = np.empty(0)
    valtable = np.empty(0)
    file = open(file, "r")
    contents = file.read()
    file_as_list = contents.splitlines()
    for line in file_as_list:
        if check_line(line) == 1:
            k, val = line.split()
            k = float(k)
            val = float(val)
            if k >= kmin and k <= kmax:
                ktable = np.append(ktable, k)
                valtable = np.append(valtable, val)

    file.close()
    # print(ktable,valtable)
    return ktable, valtable


def read_2Cfull_file(file):
    ktable = np.empty(0)
    valtable = np.empty(0)
    file = open(file, "r")
    contents = file.read()
    file_as_list = contents.splitlines()
    for line in file_as_list:
        if check_line(line) == 1:
            k, val = line.split()
            k = float(k)
            val = float(val)
            ktable = np.append(ktable, k)
            valtable = np.append(valtable, val)

    file.close()
    # print(ktable,valtable)
    return ktable, valtable


def read_FEFF_file(file, kmin, kmax):
    ktable = np.empty(0)
    valtable = np.empty(0)
    file = open(file, "r")
    contents = file.read()
    file_as_list = contents.splitlines()
    istart = 0
    toto = 0
    for line in file_as_list:
        istart = istart + 1
        #        print(line)
        if line[1:20] == "-------------------":
            toto = 1
            istart_data = istart + 2
        if toto == 1 and istart >= istart_data:  # read data
            if check_line(line) == 1:
                k, val, a, b, c = line.split()
                k = float(k)
                val = float(val)
                # print(k,val)
                if k >= kmin and k <= kmax:
                    ktable = np.append(ktable, k)
                    valtable = np.append(valtable, val)
                    # print(line ,toto)
    file.close()
    # print(ktable,valtable)
    return ktable, valtable


def build_indexinv(index):
    maxval = max(index)
    indexinv = np.zeros(maxval + 1, dtype=int)
    indexinv[:] = -1
    for i in np.arange(0, len(index)):
        # print(index[i])
        if indexinv[index[i]] != -1:
            print("problem in the list of paths, two paths have the same index")
            exit()
        indexinv[index[i]] = i
    return indexinv


def check_line(line):
    check = 0
    if line != "" and line != " " and line != "  " and line != "   " and line != "    ":
        check = 1
    return check


def shift_k_DeltaE(kfull, DeltaE, chifull, kmin, kmax):
    ktable = np.empty(0)
    valtable = np.empty(0)
    # compute the shifted version of kfull
    kfullshift = np.zeros(len(kfull))
    for i in np.arange(0, len(kfull)):
        if (kfull[i] / 0.512) ** 2 + DeltaE >= 0:
            kfullshift[i] = 0.512 * np.sqrt((kfull[i] / 0.512) ** 2 + DeltaE)
        else:
            kfullshift[i] = 0

    for i in np.arange(0, len(kfull)):
        k = kfull[i]
        if k >= kmin and k <= kmax:
            ktable = np.append(ktable, k)
            # fill the chi value, taking into account the DeltaE shift
            # 1 find the 2 values of kfullshift around k
            istart = -1
            for j in np.arange(0, len(kfull) - 1):
                if kfullshift[j] <= k and kfullshift[j + 1] >= k:
                    istart = j
                    break
            # 2 now interpolate
            if istart == -1:
                print("problem with interpolation of k values, the k interval is not well chosen. Change kmin and/or kmax in the input, code stopped now")
                exit()
            else:
                val = chifull[istart] * (kfullshift[istart + 1] - k) / (kfullshift[istart + 1] - kfullshift[istart]) + chifull[istart + 1] * (k - kfullshift[istart]) / (kfullshift[istart + 1] - kfullshift[istart])
                valtable = np.append(valtable, val)
    return ktable, valtable


def check_k(k1, k2):  # function to check the sampling of k values from the files
    n1 = len(k1)
    n2 = len(k2)
    success = 1
    if n1 != n2:
        print("problem in the k sampling of the input files, please check your inputs")
        success = 0
    if n1 == n2:
        for i in np.arange(0, n1):
            if k1[i] != k2[i]:
                print(
                    "problem in the list of k values",
                    k1[i],
                    "and ",
                    k2[i],
                    "do not match",
                )
                success = 0
    return success


def gotofourier(k, chi):
    kmin = k[0]
    kmax = k[-1]
    dk = k[1] - k[0]
    L = kmax - kmin
    # Fourier based set up
    numax = 1 / dk
    #    print(kmin,kmax,k[1],dk,numax)

    # define a Kaiser window
    kaiser = signal.windows.kaiser(len(chi), 2.5)  # beta = 2.5
    chikaiser = chi * kaiser
    # determine the number of sample for pading to get a dA=0.05 Angstrom
    Ltarget = 1 / 0.05 * np.pi  # times pi because of the link between Angstrom in physical space and k in spectro
    nLtarget = int(Ltarget / dk)
    # print('nLtarget',nLtarget,len(chi))
    chikaiserpad = np.zeros((nLtarget))
    chikaiserpad[0 : len(chi)] = chikaiser

    Fchi = np.fft.fft(chikaiserpad)
    nu = np.linspace(0, numax, len(Fchi)) * np.pi  # the *np.pi comes from the definition of k which is pi x spatial freq
    # compute ninumax: index associated to 10A
    ninumax = 0
    for i in range(len(nu)):
        if nu[i] > 10:
            # stop
            ninumax = i
            break

    # ninumax=int(len(nu)/2)
    # positive par of the spectrum
    nup = nu[0:ninumax]
    Fchip = Fchi[0:ninumax]
    return nup, Fchip


def plot_fourier(k, chi, title, icolor):
    nup, Fchip = gotofourier(k, chi)
    plt.figure(2)
    plt.plot(nup, np.abs(Fchip), color=icolor, label=title)
    plt.legend()
    plt.figure(3)
    plt.plot(nup, np.real(Fchip), color=icolor, label=title)
    plt.legend()
    return


def init_param(m, im, sig_init, opt, cor, sig_min, sig_max, indexinv, npath):
    for i in np.arange(0, npath):
        if opt[i] == 1:
            m[i, 0] = sig_init[i]  # init the value
            if cor[i] == -1:
                im[i] = 1  # inversion is performed for this parameter
            if cor[i] != -1:
                im[i] = 2  # inversion is performed for this parameter but through another parameter
                # check that the inputs are well set -> cor[cor[i]] should be -1
                if cor[indexinv[cor[i]]] != -1 or opt[indexinv[cor[i]]] != 1:
                    # problem
                    print(
                        "problem in the input, parameter is linked",
                        i,
                        "to parameter",
                        cor[i],
                    )
                    print(
                        "but parameter",
                        cor[i],
                        "is linked to another parameter or not reconstructed",
                    )
                    exit()
                # print(cor[i])
                sig_min[i] = sig_min[indexinv[cor[i]]]
                sig_max[i] = sig_max[indexinv[cor[i]]]
        if opt[i] == 0:
            im[i] = 0  # inversion is not performed for this parameter
            m[i, 0] = sig_init[i]  # fixed value
    if np.sum(opt[:]) == 0:  # problem, no any parameter is reconstructed
        print("problem, all parameters are set as 'not reconstructed', the inversion cannot start")
        print("code stopped now")
        exit()
    return m, im, sig_min, sig_max


def misfit_gradient(k, chi, chipath, weight, npath, sig, isig, cor, S02, indexinv):
    # compute misfit
    misfit = 0.0
    denom = 0.0
    chi_k = np.zeros(len(k))
    for ik in np.arange(0, len(k)):  # loop over k
        for ipath in np.arange(0, npath):  # loop over path
            chi_k[ik] = chi_k[ik] + S02 * chipath[ik, ipath] * np.exp(-2 * sig[ipath, 0] ** 2 * k[ik] ** 2)
        # update misfit
        misfit = misfit + (chi[ik] - chi_k[ik]) * weight[ik] ** 2 * (chi[ik] - chi_k[ik])
        denom = denom + chi[ik] ** 2 * weight[ik] ** 2
    misfit = misfit / denom
    # compute gradient
    gradient = np.zeros((npath, 1))
    for ipath in np.arange(0, npath):
        grad = 0.0
        if isig[ipath] >= 1:  # inversion is performed
            for ik in np.arange(0, len(k)):  # loop over k
                # compute lambda1
                lambda1 = weight[ik] ** 2 * (chi[ik] - chi_k[ik]) / denom
                # update grad
                grad = grad + 2 * lambda1 * S02 * chipath[ik, ipath] * 4 * sig[ipath, 0] * k[ik] ** 2 * np.exp(-2 * sig[ipath, 0] ** 2 * k[ik] ** 2)
                # print(grad,chipath[ik,ipath])
            # print(grad)
            if isig[ipath] == 1:  # inversion for this parameter
                gradient[ipath, 0] = gradient[ipath, 0] + grad
            if isig[ipath] == 2:  # update an other parameter
                gradient[indexinv[cor[ipath]], 0] = gradient[indexinv[cor[ipath]], 0] + grad
    # for correlated parameter, update the gradient
    for ipath in np.arange(0, npath):
        if isig[ipath] == 2:
            gradient[ipath, 0] = gradient[indexinv[cor[ipath]], 0]

    #  print(misfit,gradient)

    return misfit, gradient, chi_k


def misfit(k, chi, chipath, weight, npath, sig, S02):
    # compute misfit
    misfit = 0.0
    denom = 0.0
    chi_k = np.zeros(len(k))
    for ik in np.arange(0, len(k)):  # loop over k
        for ipath in np.arange(0, npath):  # loop over path
            chi_k[ik] = chi_k[ik] + S02 * chipath[ik, ipath] * np.exp(-2 * sig[ipath, 0] ** 2 * k[ik] ** 2)
        # update misfit
        misfit = misfit + (chi[ik] - chi_k[ik]) * weight[ik] ** 2 * (chi[ik] - chi_k[ik])
        denom = denom + chi[ik] ** 2 * weight[ik] ** 2
    misfit = misfit / denom

    return misfit


def apply_bound(mtest, sig_min, sig_max):
    for i in np.arange(0, len(mtest)):
        if mtest[i, 0] < sig_min[i]:
            mtest[i, 0] = sig_min[i]
        if mtest[i, 0] > sig_max[i]:
            mtest[i, 0] = sig_max[i]
    return mtest


def compute_linesearch_wolfe(
    m,
    f,
    grad,
    p,
    alpha_ls,
    chi,
    chipath,
    weight,
    npath,
    isig,
    cor,
    S02,
    sig_min,
    sig_max,
    indexinv,
):
    conv1 = 0  # convergence flag for first Wolfe cond
    conv2 = 0  # convergence flag for second Wolfe cond
    #    alpha=1. #initial step lenght
    c1 = 1e-4
    c2 = 0.9
    theta = 2
    gamma = 10
    kk = 0
    while conv1 == 0 or conv2 == 0:
        conv1 = 0  # convergence flag for first Wolfe cond
        # compute function and gradient for the alpha value
        mtest = m + alpha_ls * p
        mtest = apply_bound(mtest, sig_min, sig_max)
        # p=(mtest-m)/alpha_ls
        ftest, gradtest, chi_k_test = misfit_gradient(k, chi, chipath, weight, npath, mtest, isig, cor, S02, indexinv)
        if ftest < (f + c1 * alpha_ls * grad.T @ p):
            # we are happy
            conv1 = 1
            if gradtest.T @ p > c2 * grad.T @ p:
                # we are happy
                conv2 = 1
            else:
                # we need to change alpha with gamma to decrease it
                alpha_ls = alpha_ls * gamma
        else:
            # we need to change alpha with beta to increase it
            alpha_ls = alpha_ls / theta
        # print(f,ftest,mtest,alpha_ls)
        kk += 1  # just to count inner iterations of line search
        # print(kk)
    # print('kk',kk,alpha_ls)
    return alpha_ls


def compute_linesearch_wolfe_nl(
    k,
    m,
    m2,
    f,
    grad2,
    p,
    alpha_ls,
    chi,
    chipath,
    weight,
    npath,
    isig,
    cor,
    S02,
    sig_min,
    sig_max,
    indexinv,
):
    mtest = np.zeros((npath, 1))
    m2test = np.zeros((npath, 1))
    grad2test = np.zeros((npath, 1))
    gradtest = np.zeros((npath, 1))
    conv1 = 0  # convergence flag for first Wolfe cond
    conv2 = 0  # convergence flag for second Wolfe cond
    #    alpha=1. #initial step lenght
    c1 = 1e-4
    c2 = 0.9
    theta = 2
    gamma = 10
    kk = 0
    ls_status = 1
    while conv1 == 0 or conv2 == 0:
        conv1 = 0  # convergence flag for first Wolfe cond
        # compute function and gradient for the alpha value
        m2test = m2 + alpha_ls * p
        # p=(mtest-m)/alpha_ls
        mtest = nl_tranf_bwd(m, sig_min, sig_max, npath, m2test)
        ftest, gradtest, chi_k_test = misfit_gradient(k, chi, chipath, weight, npath, mtest, isig, cor, S02, indexinv)
        grad2test = nl_tranf_fwd_grad(mtest, sig_min, sig_max, npath, gradtest, grad2test)
        if ftest < (f + c1 * alpha_ls * grad2.T @ p):
            # we are happy
            conv1 = 1
            if grad2test.T @ p > c2 * grad2.T @ p:
                # we are happy
                conv2 = 1
            else:
                # we need to change alpha with gamma to decrease it
                alpha_ls = alpha_ls * gamma
        else:
            # we need to change alpha with beta to increase it
            alpha_ls = alpha_ls / theta
        # print(f,ftest,mtest,alpha_ls)
        kk += 1  # just to count inner iterations of line search
        if kk > 100:
            # line search  failure
            # print("line search failure")
            ls_status = 2
            conv1 = 1
            conv2 = 1
        # print(kk)
    # print('kk',kk,alpha_ls)
    return alpha_ls, ls_status


def non_linear_inversion(
    k,
    chi,
    chipath,
    weight,
    npath,
    m,
    im,
    cor,
    sig_min,
    sig_max,
    S02,
    ifirstcall,
    indexinv,
):
    # perform iterations on the non-linear inverse problem
    # first estimation
    f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
    if ifirstcall == 1:
        plt.figure(1)

        plt.plot(k, chi * weight, color="b", label="data")
        plt.plot(k, chi_k * weight, color="r", label="before optimization")
        ifirstcall = 2

    alpha_ls = 1e-3 * np.sqrt(np.sum(m**2) / np.sum(grad**2))
    # alpha_ls=0.01
    conv = 0  # flag for convergence
    nit = 0
    fm1 = 10 * f

    while conv < 1:
        nit += 1
        # compute the misfit value and gradient
        f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
        # steepest descent direction
        p = -grad
        # print(m,p)
        # find line-search
        alpha_ls = compute_linesearch_wolfe(
            m,
            f,
            grad,
            p,
            alpha_ls,
            chi,
            chipath,
            weight,
            npath,
            im,
            cor,
            S02,
            sig_min,
            sig_max,
            indexinv,
        )
        # update the model
        m = m + alpha_ls * p
        # print(m)
        # check convergence
        if np.abs(fm1 - f) / np.abs(fm1) < 0.000001 or nit > 2000:
            conv = 2
            # print('conv',f,m)
            # print(nit),f
        fm1 = f

        if nit % 100 == 0:
            print("iteration", nit)

    f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
    return f, chi_k, m, ifirstcall


def non_linear_inversion_nl(
    k,
    chi,
    chipath,
    weight,
    npath,
    m,
    im,
    cor,
    sig_min,
    sig_max,
    S02,
    ifirstcall,
    indexinv,
):
    # go to non-linear transformation of sigma to handle bound constraints
    m2 = np.zeros((npath, 1))
    grad2 = np.zeros((npath, 1))
    m2 = nl_tranf_fwd(m, sig_min, sig_max, npath, m2)
    # perform iterations on the non-linear inverse problem
    # first estimation (of m)
    # go back to m for gradient
    m = nl_tranf_bwd(m, sig_min, sig_max, npath, m2)
    f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
    # go to grad2
    grad2 = nl_tranf_fwd_grad(m, sig_min, sig_max, npath, grad, grad2)
    if ifirstcall == 1:
        plt.figure(1)
        # plt.plot(kfullw,chifullw*weight,color='black',label='data before DeltaE shift')

        plt.plot(k, chi * weight, color="b", label="data")
        plt.plot(k, chi_k * weight, color="r", label="before optimization")
        # go to Fourier space for plot
        plot_fourier(k, chi * weight, "data", "b")
        plot_fourier(k, chi_k * weight, "before optimization", "r")

        ifirstcall = 2

    alpha_ls = 1e-3 * np.sqrt(np.sum(m2**2) / np.sum(grad2**2))
    # alpha_ls=0.01
    conv = 0  # flag for convergence
    nit = 0
    fm1 = 10 * f

    while conv < 1:
        nit += 1
        # go back to m for gradient
        m = nl_tranf_bwd(m, sig_min, sig_max, npath, m2)
        # compute the misfit value and gradient
        f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
        # go to grad2
        grad2 = nl_tranf_fwd_grad(m, sig_min, sig_max, npath, grad, grad2)
        # steepest descent direction
        p = -grad2
        # print(m,p)
        # find line-search
        alpha_ls, ls_status = compute_linesearch_wolfe_nl(
            k,
            m,
            m2,
            f,
            grad2,
            p,
            alpha_ls,
            chi,
            chipath,
            weight,
            npath,
            im,
            cor,
            S02,
            sig_min,
            sig_max,
            indexinv,
        )
        if ls_status == 2:
            # line search failure, we stop iterating now
            conv = 3
            print(
                "stop non-linear iterations with line-search failure after ",
                nit,
                "iterations",
            )
        if ls_status == 1:
            # update the model
            m2 = m2 + alpha_ls * p
            # print(m2)
            # check convergence
            if np.abs(fm1 - f) / np.abs(fm1) < 0.000001 or nit > 2000:
                conv = 2
                # print('conv',f,m)
                # print(nit),f
            fm1 = f

        if nit % 10 == 0:
            print("iteration", nit)
    # go back to m for gradient
    m = nl_tranf_bwd(m, sig_min, sig_max, npath, m2)
    f, grad, chi_k = misfit_gradient(k, chi, chipath, weight, npath, m, im, cor, S02, indexinv)
    return f, chi_k, m, ifirstcall


def nl_tranf_fwd(m, sig_min, sig_max, npath, m2):
    for ipath in np.arange(0, npath):
        mmean = 0.5 * (sig_max[ipath] + sig_min[ipath])
        m1 = 2 * (m[ipath, 0] - mmean) / (sig_max[ipath] - sig_min[ipath])
        if m1 == -1:  # just in case to avoid inf
            m1 = m1 + np.finfo(np.float64).eps
            # print('rescale+')
        if m1 == 1:  # just in case to avoid inf
            m1 = m1 - np.finfo(np.float64).eps
            # print('rescale-')
        m2[ipath, 0] = np.arctanh(m1)
    return m2


def nl_tranf_fwd_grad(m, sig_min, sig_max, npath, grad, grad2):
    for ipath in np.arange(0, npath):
        mmean = 0.5 * (sig_max[ipath] + sig_min[ipath])
        m1 = 2 * (m[ipath, 0] - mmean) / (sig_max[ipath] - sig_min[ipath])
        if m1 == -1:  # just in case to avoid inf
            m1 = m1 + np.finfo(np.float64).eps
            # print('rescale+')
        if m1 == 1:  # just in case to avoid inf
            m1 = m1 - np.finfo(np.float64).eps
            # print('rescale-')
        m2 = np.arctanh(m1)
        # print(m[ipath,0],m1,m2)
        grad2[ipath, 0] = grad[ipath, 0] * 0.5 * (sig_max[ipath] - sig_min[ipath]) * (1 - np.tanh(m2) ** 2)
    return grad2


def nl_tranf_bwd(m, sig_min, sig_max, npath, m2):
    for ipath in np.arange(0, npath):
        m1 = np.tanh(m2[ipath, 0])
        mmean = 0.5 * (sig_max[ipath] + sig_min[ipath])
        m[ipath, 0] = m1 / 2 * (sig_max[ipath] - sig_min[ipath]) + mmean
    return m


def main():
    ###############################
    # Management of the output log.
    ###############################
    if len(sys.argv) != 2:
        print("Usage: dft2feffit inputfile")
        exit()

    print("***********************")
    print("       DFT2FEFFIT      ")
    print("***********************")
    print("Authors:")
    print("    Romain Brossier (romain.brossier@univ-grenoble-alpes.fr)")
    print("    Alain Manceau (alain.manceau@ens-lyon.fr)")
    print(f"Version: {version}")
    # print("Release date : 9 February 2024")
    # print("The article describing the software can be found here:")
    # print("https://doi.org/XXXXX")
    # print("The Python source can be found here: https://gitlab.esrf.fr/scientific-software/dft2feffit")
    now = datetime.datetime.now()
    print(f"Run started: {now.strftime('%Y-%m-%d %H:%M:%S')}")
    print("***********************")

    #############################
    # Management of the inputs
    #############################
    input_path = pathlib.Path(sys.argv[1])
    parent_path = input_path.parent
    with open(input_path, "r") as fh:
        input_parameters = fh.read().splitlines()

    filechi = str(input_parameters[0])
    print("EXAFS spectrum to fit:", filechi)

    npath = int(input_parameters[1])
    print("Number of paths =", npath)

    kweight = float(input_parameters[2])
    print("Power of k =", kweight)

    line = input_parameters[3]
    kmin, kmax = line.split()
    kmin = float(kmin)
    kmax = float(kmax)
    print("kmin, kmax =", kmin, kmax)

    S02 = float(input_parameters[4])
    print("S02 =", S02)

    iDeltaE = int(input_parameters[5])
    print("DeltaE variable (1) or fixed (0) =", iDeltaE)

    if iDeltaE == 0:
        DeltaE = float(input_parameters[6])
        print("DeltaE =", DeltaE)
    if iDeltaE == 1:
        line = input_parameters[6]
        DeltaEmin, DeltaEmax, DeltaDeltaE = line.split()
        DeltaEmin = float(DeltaEmin)
        DeltaEmax = float(DeltaEmax)
        DeltaDeltaE = float(DeltaDeltaE)
        print("min, max, and step of DeltaE =", DeltaEmin, DeltaEmax, DeltaDeltaE)
    print("***********************")
    (
        index,
        paths,
        distance,
        files,
        form,
        opt,
        sig_init,
        sig_min,
        sig_max,
        cor,
    ) = read_inputs(input_parameters, npath)

    # build indexinv
    indexinv = build_indexinv(index)

    #############################
    # Read data files
    #############################
    # Read observed data to be fit
    filechi_path = os.path.join(parent_path, filechi)
    kfull, chifull = read_2Cfull_file(filechi_path)
    # kfullw,chifullw=shift_k_DeltaE(kfull,0.,chifull,kmin,kmax)
    if iDeltaE == 1:
        DeltaE = DeltaEmin

    k, chi = shift_k_DeltaE(kfull, DeltaE, chifull, kmin, kmax)

    nk = len(k)

    # read files related to paths
    kpath = np.empty((nk, npath))
    chipath = np.empty((nk, npath))

    for i in np.arange(0, npath):
        file_path = os.path.join(parent_path, files[i])
        if form[i] == "2C":  # 2 column format
            ktmp, chitmp = read_2C_file(file_path, kmin, kmax)

        if form[i] == "FEFF":  # FEFF format
            ktmp, chitmp = read_FEFF_file(file_path, kmin, kmax)

        success = check_k(ktmp, k)
        if success == 1:
            kpath[:, i] = ktmp
            chipath[:, i] = chitmp
            print("file ", files[i], "Read with success")
        if success == 0:
            print("")
            print("file ", files[i], "cannot be read, code stopped now")
            exit()

    # print(chipath)

    # init figures
    plt.figure(1)
    plt.title("fit in k space")
    plt.xlabel("k")
    tmpchar = "k^" + str(kweight) + "* chi"
    plt.ylabel(tmpchar)

    plt.figure(2)
    plt.title("fit in real space - modulus")
    plt.xlabel("Angström")
    plt.xlim([0, 6])
    plt.ylabel("modulus")

    plt.figure(3)
    plt.title("fit in real space - real part")
    plt.xlabel("Angström")
    plt.xlim([0, 6])
    plt.ylabel("real amplitude")

    print("***********************")
    print("   START OPTIMIZATION")
    print("***********************")
    #############################
    # perform inversion on sigma
    #############################
    # define weight
    weight = np.empty(nk)
    for i in np.arange(0, nk):
        weight[i] = k[i] ** kweight

    # init parameters
    m = np.empty((npath, 1))
    im = np.empty(npath)
    DeltaEbest = DeltaE

    if iDeltaE == 0:
        ifirstcall = 1
        k, chi = shift_k_DeltaE(kfull, DeltaE, chifull, kmin, kmax)
        m, im, sig_min, sig_max = init_param(m, im, sig_init, opt, cor, sig_min, sig_max, indexinv, npath)
        f, chi_k, m, ifirstcall = non_linear_inversion_nl(
            k,
            chi,
            chipath,
            weight,
            npath,
            m,
            im,
            cor,
            sig_min,
            sig_max,
            S02,
            ifirstcall,
            indexinv,
        )
        print(f)

    if iDeltaE == 1:
        ifirstcall = 2  # no ploting for now
        fbest = 1.0e30
        chi_kbest = np.zeros(len(k))
        m_best = np.empty((npath, 1))
        DeltaEbest = DeltaEmin
        for DeltaE in np.arange(DeltaEmin, DeltaEmax + DeltaDeltaE, DeltaDeltaE):
            print("")
            print("Testing DeltaE =", DeltaE)
            k, chi = shift_k_DeltaE(kfull, DeltaE, chifull, kmin, kmax)
            m, im, sig_min, sig_max = init_param(m, im, sig_init, opt, cor, sig_min, sig_max, indexinv, npath)
            f, chi_k, m, ifirstcall = non_linear_inversion_nl(
                k,
                chi,
                chipath,
                weight,
                npath,
                m,
                im,
                cor,
                sig_min,
                sig_max,
                S02,
                ifirstcall,
                indexinv,
            )
            if f < fbest:
                fbest = f
                chi_kbest[:] = chi_k[:]
                m_best[:, 0] = m[:, 0]
                DeltaEbest = DeltaE
            print("For DeltaE=", DeltaE, ", normalized sum-sq residual =", f)
            print("")
            # print(m)
        DeltaEmax = DeltaE  # reinit DeltaEmax as the last one tested
        ifirstcall = 1  # now plot
        DeltaE = DeltaEbest
        print("Recompute final values for DeltaE=", DeltaE)

        k, chi = shift_k_DeltaE(kfull, DeltaE, chifull, kmin, kmax)
        m, im, sig_min, sig_max = init_param(m, im, sig_init, opt, cor, sig_min, sig_max, indexinv, npath)
        f, chi_k, m, ifirstcall = non_linear_inversion_nl(
            k,
            chi,
            chipath,
            weight,
            npath,
            m,
            im,
            cor,
            sig_min,
            sig_max,
            S02,
            ifirstcall,
            indexinv,
        )

    # PLOT THE BEST SOLUTION
    print("***********************")
    print("   RESULTS             ")
    print("***********************")
    print("")
    print("Fit of ", filechi)
    print("")
    print("Best normalized sum-sq residual", f)
    print("")
    print("DeltaE value", DeltaE)
    print("")
    if iDeltaE == 1 and (DeltaE == DeltaEmin or DeltaE == DeltaEmax):
        print("WARNING. The best DeltaE value is on the bounds of the specified values")
        print("")
    print("Best Sigma values")
    for i in np.arange(0, npath):
        print(
            "path index",
            index[i],
            "with path name",
            paths[i],
            "with path distance",
            distance[i],
            "optimal Sigma after inversion",
            m[i, 0],
        )
    print("")
    print("Sum up of Sigma values")
    print(m)
    print("")
    print("Best model fit in k space")
    tmpchar = "k ;   k^" + str(kweight) + "*chi exp shifted ;  k^" + str(kweight) + "*chi calc."
    print(tmpchar)
    for i in np.arange(0, len(k)):
        print(k[i], " ; ", chi[i] * weight[i], " ; ", chi_k[i] * weight[i])
    print("")
    print("Best model fit in real space (Kaiser window with beta=2.5)")
    nup, Fchip = gotofourier(k, chi * weight)
    nup, Fchi_kp = gotofourier(k, chi_k * weight)
    tmpchar = "Dist (A);   Modulus FT(k^" + str(kweight) + "*chi exp shifted) ;  Modulus FT (k^" + str(kweight) + "*chi calc) ; Real FT(k^" + str(kweight) + "*chi exp shifted) ;  Real FT (k^" + str(kweight) + "*chi calc)"
    print(tmpchar)
    for i in np.arange(0, len(nup)):
        print(
            nup[i],
            " ; ",
            np.abs(Fchip[i]),
            " ; ",
            np.abs(Fchi_kp[i]),
            " ; ",
            np.real(Fchip[i]),
            " ; ",
            np.real(Fchi_kp[i]),
        )
    print("")

    # Manage output.

    input_stem = input_path.stem
    fileoutput = os.path.join(parent_path, "output_" + input_stem + ".log")
    imageoutputk = os.path.join(parent_path, "image_" + input_stem + "_k.png")
    imageoutputrealmod = os.path.join(parent_path, "image_" + input_stem + "_real_modulus.png")
    imageoutputrealr = os.path.join(parent_path, "image_" + input_stem + "_real_realpart.png")

    file = open(fileoutput, "w")
    file.write("***********************\n")
    file.writelines(now.strftime("%H:%M:%S %d/%m/%Y "))
    file.write("\n")
    file.write("\n")
    file.writelines(["Input file: ", str(input_path.name), " \n"])
    file.write("\n")
    file.writelines(["Fit of: ", str(filechi), " \n"])
    file.write("\n")
    file.write("   RESULTS\n")
    file.write("\n")
    file.writelines(["Best normalized sum-sq residual = ", str(f), "\n"])
    file.write("\n")
    file.writelines(["DeltaE = ", str(DeltaE), "\n"])
    file.write("\n")
    if iDeltaE == 1 and (DeltaE == DeltaEmin or DeltaE == DeltaEmax):
        file.write("WARNING. DeltaE did not reach a minimum \n")
    file.write("\n")
    file.write("Sigma values\n")
    for i in np.arange(0, npath):
        file.writelines(
            [
                "path index ",
                str(index[i]),
                " with path name ",
                str(paths[i]),
                " with path distance ",
                str(distance[i]),
                " optimal Sigma after inversion ",
                str(m[i, 0]),
                "\n",
            ]
        )
    file.write("\n")
    file.write("List of Sigma values\n")
    file.write(str(m))
    file.write("\n")
    file.write("\n")
    file.write("Best model fit in k space\n")
    tmpchar = "k ;   k^" + str(kweight) + "*chi exp shifted ;  k^" + str(kweight) + "*chi calc.\n"
    file.write(tmpchar)
    for i in np.arange(0, len(k)):
        file.writelines(
            [
                str(k[i]),
                " ; ",
                str(chi[i] * weight[i]),
                " ; ",
                str(chi_k[i] * weight[i]),
                "\n",
            ]
        )
    file.write("\n")
    file.write("Best model fit in real space (Kaiser window with beta=2.5)\n")
    tmpchar = "Dist (A);   Modulus FT(k^" + str(kweight) + "*chi exp shifted) ;  Modulus FT (k^" + str(kweight) + "*chi calc) ; Real FT(k^" + str(kweight) + "*chi exp shifted) ;  Real FT (k^" + str(kweight) + "*chi calc)\n"
    file.write(tmpchar)
    for i in np.arange(0, len(nup)):
        file.writelines(
            [
                str(nup[i]),
                " ; ",
                str(np.abs(Fchip[i])),
                " ; ",
                str(np.abs(Fchi_kp[i])),
                " ; ",
                str(np.real(Fchip[i])),
                " ; ",
                str(np.real(Fchi_kp[i])),
                " \n",
            ]
        )
    file.close()

    # figure 1 for fit
    plt.figure(1)
    plt.plot(k, chi_k * weight, color="g", label="after optimization")
    plt.legend()
    xvals, yvals = plt.gca().axes.get_xlim(), plt.gca().axes.get_ylim()
    xrange = xvals[1] - xvals[0]
    yrange = yvals[1] - yvals[0]
    plt.gca().set_aspect(0.8 * (xrange / yrange), adjustable="box")
    plt.savefig(imageoutputk, format="png")

    # figure 2 for fit in Fourier space
    plot_fourier(k, chi_k * weight, "after optimization", "g")
    plt.figure(2)
    xvals, yvals = plt.gca().get_xlim(), plt.gca().get_ylim()
    xrange = xvals[1] - xvals[0]
    yrange = yvals[1] - yvals[0]
    plt.gca().set_aspect(0.8 * (xrange / yrange), adjustable="box")
    plt.savefig(imageoutputrealmod, format="png")

    plt.figure(3)
    xvals, yvals = plt.gca().get_xlim(), plt.gca().get_ylim()
    xrange = xvals[1] - xvals[0]
    yrange = yvals[1] - yvals[0]
    plt.gca().set_aspect(0.8 * (xrange / yrange), adjustable="box")
    plt.savefig(imageoutputrealr, format="png")

    plt.show()


if __name__ == "__main__":
    main()
